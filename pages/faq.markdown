## Frequently Asked Questions

### What's new?

After a week end of furious coding I put together some features I was thinking about for a long time. With the new version you can have suggestions for a keyword just by clicking on it. You can also add a keyword to a basket and download them later.

I'm sure there are bugs I'm not aware of and the look & feel could be improved so if you stumble upon any problems, [just tell me](https://bitbucket.org/esaurito/ubersuggest/issues/new).

### Could you add feature XYZ?

I am happy to receive suggestions and feature request from users. The best
place to add a feature request is [this page on
Bitbucket](https://bitbucket.org/esaurito/ubersuggest/issues/new) (note that
you *do not* need to be registered on Bitbucket to submit an issue, just fill
the form and you are OK).  

Anyway keep in mind that I develop and maintain Übersuggest in my spare time,
so *do not expect that your request will be implemented in a short time* (if
ever).

By the way money is the best way to speed up the development process ;-)

### Can you add my language?

Yes. Just add a new language request [here on
Bitbucket](https://bitbucket.org/esaurito/ubersuggest/issues/new) (note that
you *do not* need to be registered on Bitbucket to submit a request, just fill
the form and you are OK).  

### I would like to build my own Übersuggest. Could you help me?

Sure. Übersuggest is open source ad it is implemented with
[Python](http://www.python.org/) and [Flask](http://flask.pocoo.org/). Python
is really easy to understand even if you do not know the language syntax.  The
best thing you can do to start building your own tool is to go
[here](https://bitbucket.org/esaurito/ubersuggest/src) and look at the code. It
is very simple, trust me.

### Your tool saved my life! What can I do to show my appreciation?

Buy me a beer using the PayPal button on the home page.


### Who are you?

I am an [Italian SEO](http://it.linkedin.com/in/alemartin) now working with [Sems](http://www.sems.it). I have a background in sociology and usability and I have fun programming small (Web) application in Python. Contacts: `thinkpragmatic at gmail dot com`. 
