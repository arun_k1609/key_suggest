from werkzeug import parse_date
from datetime import datetime, timedelta

def format_timedelta(delta, granularity='second', threshold=.85):

    TIMEDELTA_UNITS = (
        ('year', 3600 * 24 * 365),
        ('month', 3600 * 24 * 30),
        ('week', 3600 * 24 * 7),
        ('day', 3600 * 24),
        ('hour', 3600),
        ('minute', 60),
        ('second', 1)
    )
    delta = parse_date(delta)
    if isinstance(delta, datetime):
        delta = datetime.utcnow() - delta
    if isinstance(delta, timedelta):
        seconds = int((delta.days * 86400) + delta.seconds)
    else:
        seconds = delta

    for unit, secs_per_unit in TIMEDELTA_UNITS:
        value = abs(seconds) / secs_per_unit
        if value >= threshold or unit == granularity:
            if unit == granularity and value > 0:
                value = max(1, value)
            value = int(round(value))
            rv = u'%s %s' % (value, unit)
            if value != 1:
                rv += u's'
            return rv
    return u''
