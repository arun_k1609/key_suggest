# vim: set fileencoding=utf-8
from flask import Flask, render_template, request, flash, send_file, Markup, jsonify, session, url_for
from urllib import quote
from string import ascii_lowercase as alpha
from tempfile import NamedTemporaryFile
from operator import itemgetter
import codecs
import markdown
import os
from languages import LANGUAGES, HIRAGANA, get_language_by_name
import twitter
from utils import format_timedelta
import random
import requests
#from flaskext.zodb import ZODB
from datetime import datetime


try:
    import json
except ImportError:
    import simplejson as json

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
app = Flask(__name__)
#db = ZODB(app)

#@app.before_request
#def set_db_defaults():
#    if 'suggestions' not in db:
#        db['suggestions'] = List()
#        for language in LANGUAGES:
#            code = language['code']
#            db['suggestions'][code] = {}
app.config.from_pyfile('settings.py')
app.jinja_env.filters['timedeltaformat'] = format_timedelta
proxy_list = app.config.get('PROXY_LIST', [])
#Set the user agent to a common browser user agent string to get always utf-8 encoded response
headers = {'User-agent':'Mozilla/5.0'}
languages = sorted(LANGUAGES, key=itemgetter('name'))
#Alternative URL
#www.google.com/complete/search?hjson=true&qu=a

#api = twitter.Api()
#api.SetCacheTimeout(120)

def get_suggestion(query, lang, tld, ds=''):
    """Query Google suggest service"""
    suggestions = []
    if query:
        if isinstance(query, unicode): 
            query = query.encode('utf-8')
        query = quote(query)
        url = "http://clients1.google.%s/complete/search?hl=%s&q=%s&json=t&ds=%s&client=serp" %(tld, lang, query, ds)
        the_url = random.choice(proxy_list)
        response = requests.get(the_url, headers=headers, params={'url':url})
        if response.ok:
            result = json.loads(response.content)
            suggestions = [i for i in result[1]]
        else:
            pass #FIXME handle and display errors
    return suggestions

def get_news_suggestion(query, lang, country):
    suggestions = []
    if query:
        if isinstance(query, unicode): 
            query = query.encode('utf-8')
        query = quote(query)
        url = "http://news.google.com/complete/search?hl=%s&gl=%s&ds=n&nolabels=t&hjson=t&client=news&q=%s" %(lang, country, query)
        the_url = random.choice(proxy_list)
        response = requests.get(the_url, headers=headers, params={'url':url})
        if response.ok:
            result = json.loads(response.content)
            suggestions = [i[0] for i in result[1]]
    return suggestions

@app.route("/_suggest", methods=['GET'])
def suggest():
    """Provide suggestions via AJAX"""
    query = request.args.get('word', '', type=str)
    source = request.args.get('source', '', type=str)
    language = session.get('language')
    result = []
    if query and source and language: 
        if source == 'web':
            result = get_suggestion(query, language['code'], language['tld'])
        elif source == 'pr':
            result = get_suggestion(query, language['code'], language['tld'], ds='pr')
        elif source == 'news':
            result = get_news_suggestion(query, language['code'], language['country'])
    return jsonify(suggestions = result[1:])

@app.route("/faq/", methods=['GET'])
def show_faq():
    """Show a brief FAQ"""
    path = os.path.join(ROOT_DIR, 'pages/faq.markdown')
    content = codecs.open(path,'r','utf-8').read()
    content = Markup(markdown.markdown(content))
    return render_template('faq.html', content=content)

@app.route("/", methods=['GET', 'POST'])
def prompt():
    """Get suggestion and expand the query """

    format = request.values.get('format')
    query = request.values.get('query', '')
    selected_language = get_language_by_name(request.values.get('language'))
    session['language'] = selected_language

    if (request.method == 'POST' or format) and query and selected_language:
        expansion = []
        expanded = request.values.get('expand')
        source = request.values.get('source')
        chars = list(alpha)
        chars.extend(map(str,range(10)))
        if selected_language['code'] == 'ja':
            chars = ascii_lowercase + HIRAGANA
        try:
            if source == 'web':
                gweb = get_suggestion(query, selected_language['code'], selected_language['tld'])
            elif source == 'pr':
                gweb = get_suggestion(query, selected_language['code'], selected_language['tld'], ds='pr')
            elif source == 'news':
                gweb = get_news_suggestion(query, selected_language['code'], selected_language['country'])
            for letter in chars:
                suf_query = query + ' ' + letter
                pre_query = letter + ' ' + query
                if source == 'web':
                    suf_suggestions = get_suggestion(suf_query, selected_language['code'], selected_language['tld'])
                    pre_suggestions = get_suggestion(pre_query, selected_language['code'], selected_language['tld'])
                elif source == 'pr':
                    suf_suggestions = get_suggestion(suf_query, selected_language['code'], selected_language['tld'], ds='pr')
                    pre_suggestions = get_suggestion(pre_query, selected_language['code'], selected_language['tld'], ds='pr')
                elif source == 'news':
                    suf_suggestions = get_news_suggestion(suf_query, selected_language['code'], selected_language['country'])
                    pre_suggestions = get_news_suggestion(pre_query, selected_language['code'], selected_language['country'])
                if pre_suggestions or suf_suggestions:
                    expansion.append((letter, pre_suggestions,suf_suggestions))

            if request.form.get('txt'):
                tmpfile = NamedTemporaryFile()
                tmpfile.write('\n'.join([i.encode('utf-8') for i in gweb]))
		file_content = []
		for letter,suggestion in expansion:
			file_content.extend(suggestion)
                tmpfile.write('\n'.join([i.encode('utf-8') for i in file_content]))
                return send_file(tmpfile.name, as_attachment=True, attachment_filename='suggestion.txt')

        except (IOError, ValueError), e:
            gweb = ''
            expansion = ''
            flash(str(e), 'error')
        data = {'result':gweb, 'expansion':expansion, 'date':datetime.now()}
        code = selected_language['code']

       # if query in db['suggestions'][code]:
            #check for difference here
        #    pass
        #else:
        #    db['suggestions'][code][query] = data
        session['basket'] = []

        url = url_for('prompt', 
                format='html', 
                language=selected_language['name'], 
                query=query, 
                expand=expanded, 
                source=source, 
                _external=True)

        return render_template('index.html', 
                query=query, 
                data=data, 
                languages=languages,
                url=url, 
                selected_language=selected_language, 
                source=source)

    else:
        #try:
        #	tweets = api.GetUserTimeline('ubersuggest', count=4)
        #except:
         #   tweets= []
        return render_template('index.html', data="", languages=languages, selected_language={})

if not app.debug:
    email = app.config.get('ADMIN_EMAIL')
    password = app.config.get('PASSWORD')
    username = app.config.get('ADMIN_USERNAME')
    smtp = app.config.get('SMTP')

    import logging
    from logging import Formatter
    from logging.handlers import SMTPHandler


    mail_handler = SMTPHandler(smtp,
                               'marvin@thinkpragmatic.net',
                               email, 'Ubersuggest Failed', (username, password))
    mail_handler.setLevel(logging.ERROR)


    mail_handler.setFormatter(Formatter('''
    Message type:       %(levelname)s
    Location:           %(pathname)s:%(lineno)d
    Module:             %(module)s
    Function:           %(funcName)s
    Time:               %(asctime)s

    Message:

    %(message)s
    '''))

    app.logger.addHandler(mail_handler)

if __name__ == "__main__":
    app.run()
